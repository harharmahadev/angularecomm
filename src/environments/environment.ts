// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'ecommerceapp-bca52',
    appId: '1:343708476529:web:b68920845f196ebe4de7d0',
    storageBucket: 'ecommerceapp-bca52.appspot.com',
    apiKey: 'AIzaSyBIY0pD_6bGmlh2X6v2JjsmeB7-x4Yfbuc',
    authDomain: 'ecommerceapp-bca52.firebaseapp.com',
    messagingSenderId: '343708476529',
    measurementId: 'G-RMVTDXFSZV',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
