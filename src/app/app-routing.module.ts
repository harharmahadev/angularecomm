import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LandingComponent } from './components/landing/landing.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ProductlistComponent } from './components/productlist/productlist.component';
import { ProductdetailComponent } from './components/productdetail/productdetail.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { OrderlistComponent } from './components/orderlist/orderlist.component';

import {
  canActivate,
  redirectLoggedInTo,
  redirectUnauthorizedTo,
} from '@angular/fire/auth-guard';
import { ProductComponent } from './components/product/product.component';
// import { ProfileComponent } from './components/profile/profile.component';

const redirectToLogin = () => redirectUnauthorizedTo(['login']);
const redirectToHome = () => redirectLoggedInTo(['home']);

const routes: Routes = [
  {
    path:'',
    pathMatch:'full',
    component:LandingComponent    
  },
  {
    path:'login',
    component:LoginComponent,
    ...canActivate(redirectToHome),    
  },
  {
    path:'sign-up',
    component:SignupComponent,
    ...canActivate(redirectToHome),   
  },
  {
    path:'home',
    component:HomeComponent,
    ...canActivate(redirectToLogin), 
  },
  {
    path:'profile',
    component:ProfileComponent,
    ...canActivate(redirectToLogin), 
  },
  {
    path:'product',
    component:ProductComponent,
    ...canActivate(redirectToLogin), 
  },
  {
    path:'productlist',
    component:ProductlistComponent,
    ...canActivate(redirectToLogin), 
  },
  {
    path:'productdetail',
    component:ProductdetailComponent,
    ...canActivate(redirectToLogin), 
  },
  {
    path:'checkout',
    component:CheckoutComponent,
    ...canActivate(redirectToLogin), 
  },
  {
    path:'orderlist',
    component:OrderlistComponent,
    ...canActivate(redirectToLogin), 
  }
  

];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
