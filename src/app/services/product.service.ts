import { Injectable } from '@angular/core';
import { Product } from '../models/product'; 
import {
  collection,
  doc,
  docData,
  Firestore,
  getDoc,
  setDoc,
  updateDoc,
} from '@angular/fire/firestore';
import { filter, from, map, Observable, of, switchMap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private firestore: Firestore,
  ) { }

  addProduct(product: Product): Observable<void> {
    const ref = doc(this.firestore, 'product', product.id);
    return from(setDoc(ref, product));
  }


}
