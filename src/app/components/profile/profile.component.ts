import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NonNullableFormBuilder } from '@angular/forms';
import { HotToastService } from '@ngneat/hot-toast';
import { User } from '@angular/fire/auth';
// import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { switchMap, tap,concatMap} from 'rxjs';
// import { ProfileUser } from 'src/app/models/user';
// import { ImageUploadService } from 'src/app/services/image-upload.service';
import { UsersService } from 'src/app/services/users.service';
import { AuthenticationService } from 'src/app/service/authentication.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})

export class ProfileComponent implements OnInit {
  user$ = this.authService.currentUser$;

  profileForm = this.fb.group({
    uid: new FormControl(''),
    displayName: new FormControl(''),
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    // phone: [''],
    // address: [''],
  });

  constructor(
    private authService: AuthenticationService,
    // private imageUploadService: ImageUploadService,
    private toast: HotToastService,
    private usersService: UsersService,
    private fb: NonNullableFormBuilder
  ) {}

  ngOnInit(): void {
    // this.usersService.currentUser$
    //   .pipe(untilDestroyed(this), tap(console.log))
    //   .subscribe((user) => {
    //     this.profileForm.patchValue({ ...user });
    //   });
  }


  saveProfile() {
    const { uid, ...data } = this.profileForm.value;

    if (!uid) {
      return;
    }    
  }

  getUsers() {
    // this.usersService.getUsersFromFirebase().snapshotChanges().forEach(userSnapshot => {
    //   this.userList = [];
    //   userSnapshot.forEach(userSnapshot =>{
    //     let userSnapshot.payload.toJSON();
    //     user['$key'] = userSnapshot.key;
    //     this.userList.push(user as IUser);
    //   });

    // });
  }

}