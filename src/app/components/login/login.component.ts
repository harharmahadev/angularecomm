import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { Router } from '@angular/router';
import{ HotToastService } from '@ngneat/hot-toast';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    email: new FormControl('',[Validators.required]),
    password: new FormControl('',[Validators.required]),
  })

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private toast:HotToastService
  ) { }

  ngOnInit(): void {
  }
  get email(){
    return this.loginForm.get('email');
  }
  get password(){
    return this.loginForm.get('password');
  }
  submit(){
    if(!this.loginForm.valid){
      return;
    }

    // const { email , password } = this.loginForm.value;
    console.log(this.loginForm.value.email);

    


    this.authService.login(String(this.loginForm.value.email) , String(this.loginForm.value.password) ).pipe(
        this.toast.observe({
          success:'login is successgully',
          loading : 'logging in..',
          error:'There was an error'
        })
    ).subscribe(() =>{
      this.router.navigate(['/home']);
    });

  }

}
