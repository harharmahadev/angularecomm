import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  NonNullableFormBuilder,
  ValidationErrors,
  ValidatorFn,
  Validators,
  FormsModule
} from '@angular/forms';
import { HotToastService } from '@ngneat/hot-toast';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {


  constructor(private HttpClient: HttpClient) {
      
  }

  ngOnInit(): void {

  }


  onSubmit(contactForm:any) {
      console.log(contactForm.value);

      this.HttpClient.post(
        'https://ecommerceapp-bca52-default-rtdb.firebaseio.com/product.json',
         contactForm.value
      ).subscribe(() => {
          console.log(Response);
      });
    }

  // // Fetch Students List
  // GetStudentsList() {
  //   this.contactForm = this.db.list('product');
  //   return this.contactForm;
  // }  

}
